package guy.droid.com.gridviewadapter;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ArrayList<String> name;
    ArrayList<String> image_url;
    GridView gridView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gridView = (GridView)findViewById(R.id.gridView1);
        name = new ArrayList<>();
        image_url = new ArrayList<>();
        name.add("MVD");
        name.add("KSEB");
        name.add("KWD");
        name.add("KSRTC");
        image_url.add("https://i.ytimg.com/i/NBNNpwtsTTapg70kvg9PsQ/mq1.jpg?v=4f224944");
        image_url.add("http://www.mec.ac.in/mec/images/companylogo/kseb.jpg");
        image_url.add("http://www.ennarbiz.com/images/clientele/clientele/kerala_water_authority.png");
        image_url.add("https://upload.wikimedia.org/wikipedia/en/7/71/Kerala_State_Road_Transport_Corporation_logo.png");
        gridView.setAdapter(new CustomGrid(MainActivity.this, image_url, name));
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getApplicationContext(),""+position,Toast.LENGTH_SHORT).show();
            }
        });

    }
}
