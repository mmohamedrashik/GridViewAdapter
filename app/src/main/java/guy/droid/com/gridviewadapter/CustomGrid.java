package guy.droid.com.gridviewadapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;

/**
 * Created by admin on 5/24/2016.
 */
public class CustomGrid extends ArrayAdapter<String> {
    private final ArrayList img,menu_name;
    private final Activity context;
    String mname = "";
    public CustomGrid(Activity context, ArrayList<String> img, ArrayList<String> menu_name) {
        super(context, R.layout.grid_indiv,menu_name);
        this.context = context;
        this.img = img;
        this.menu_name = menu_name;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView= inflater.inflate(R.layout.grid_indiv, null, true);
        ImageView imageView = (ImageView)rowView.findViewById(R.id.imageView1);
        TextView textView = (TextView)rowView.findViewById(R.id.textView1);
        textView.setText(""+menu_name.get(position));

        Glide.with(context)
                .load(img.get(position))
                .centerCrop()
                .into(imageView);
        return rowView;
    }
}
